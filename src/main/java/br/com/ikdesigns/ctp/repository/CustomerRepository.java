package br.com.ikdesigns.ctp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ikdesigns.ctp.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {	

	List<Customer> findByName(String name);

}
