package br.com.ikdesigns.ctp.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.com.ikdesigns.ctp.model.Customer;

public class CustomerDTO {
	
	private Integer id;
	private String name;
	private String phone;
	private String email;
	private String document;
	private String state;
	private String city;
	private String district;
	private String address;
	private String number;
	private String status;	
	
	public CustomerDTO(Customer customer) {		
		this.id = customer.getId();
		this.name = customer.getName();
		this.phone = customer.getPhone();
		this.email = customer.getEmail();
		this.document = customer.getDocument();
		this.state = customer.getState();
		this.city = customer.getCity();
		this.district = customer.getDistrict();
		this.address = customer.getAddress();
		this.number = customer.getNumber();
		this.status = customer.getStatus();
	}
	
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String getEmail() {
		return email;
	}
	public String getDocument() {
		return document;
	}
	public String getState() {
		return state;
	}
	public String getCity() {
		return city;
	}
	public String getDistrict() {
		return district;
	}
	public String getAddress() {
		return address;
	}
	public String getNumber() {
		return number;
	}
	public String getStatus() {
		return status;
	}

	public static List<CustomerDTO> converter(List<Customer> customers) {		
		return customers.stream().map(CustomerDTO::new).collect(Collectors.toList());
	}
	
	
	
	

}
