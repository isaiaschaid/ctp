package br.com.ikdesigns.ctp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ikdesigns.ctp.dto.CustomerDTO;
import br.com.ikdesigns.ctp.model.Customer;
import br.com.ikdesigns.ctp.repository.CustomerRepository;

@RestController
public class CustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@RequestMapping("/customers")
	public List<CustomerDTO> listing(String name){
		
		List<Customer> customers;
		
		if(name == null) {		
			customers = customerRepository.findAll();
		}else {
			customers = customerRepository.findByName(name);
		}
		
		return CustomerDTO.converter(customers);
	}

}
