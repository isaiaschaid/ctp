package br.com.ikdesigns.ctp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String phone;
	private String email;
	private String document;
	private String state;
	private String city;
	private String district;
	private String address;
	private String number;
	private String status;
	
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String getEmail() {
		return email;
	}
	public String getDocument() {
		return document;
	}
	public String getState() {
		return state;
	}
	public String getCity() {
		return city;
	}
	public String getDistrict() {
		return district;
	}
	public String getAddress() {
		return address;
	}
	public String getNumber() {
		return number;
	}
	public String getStatus() {
		return status;
	}	
	
	
}
